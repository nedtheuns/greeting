//
//  greeting.swift
//  test
//
//  Created by Theuns Potgieter on 2021/07/02.
//

import SwiftUI

public struct greeting: View {
    private var name: String
    
    public init(_ name: String) {
        self.name = name
    }
    
    public var body: some View {
        Text("Hello \(name). Welcome to our component.")
    }
}

struct greeting_Previews: PreviewProvider {
    static var previews: some View {
        greeting("Theuns")
    }
}
