    import XCTest
    @testable import greeting

    final class greetingTests: XCTestCase {
        func testExample() {
            // This is an example of a functional test case.
            // Use XCTAssert and related functions to verify your tests produce the correct
            // results.
            XCTAssertEqual(greeting().text, "Hello, World!")
        }
    }
